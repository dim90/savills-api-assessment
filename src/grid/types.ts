export type GridConfigs = {
  n: number;
  x: string;
  y: string;
};

export type GenerateGridParams = {
  rowSize: number;
  columnSize: number;
  initialChar: string;
  altChar: string;
};
