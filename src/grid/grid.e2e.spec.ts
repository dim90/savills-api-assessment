import { INestApplication } from "@nestjs/common";
import { Test } from "@nestjs/testing";
import supertest from "supertest";

import { GridModule } from "./grid.module";

describe("Generate Grid", () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [GridModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it("invokes POST /grid with a custom body", async () => {
    const { n, x, y } = { n: 10, x: "^", y: "-" };
    await supertest(app.getHttpServer())
      .post("/grid")
      .set("Accept", "application/json")
      .send({ n, x, y })
      .expect("Content-Type", "text/plain; charset=utf-8")
      .expect(200)
      .then(async ({ text }: { text: string }) => {
        expect(text.trim().split("\n").length).toBe(n);
        expect(text).toEqual(expect.stringMatching(/\^-/));
      });
  });

  it("invokes POST /grid without any body and returns a grid with the default values", async () => {
    const defaultGridNValue = 8;
    await supertest(app.getHttpServer())
      .post("/grid")
      .set("Accept", "application/json")
      .send()
      .expect("Content-Type", "text/plain; charset=utf-8")
      .expect(200)
      .then(async ({ text }: { text: string }) => {
        expect(text.trim().split("\n").length).toBe(defaultGridNValue);
        expect(text).toEqual(expect.stringMatching(/# /));
      });
  });

  afterAll(async () => {
    await app.close();
  });
});
