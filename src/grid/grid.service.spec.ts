import { Test, TestingModule } from "@nestjs/testing";

import { GridService } from "./grid.service";
import { GenerateGridParams } from "./types";

describe("GridService", () => {
  let service: GridService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GridService],
    }).compile();

    service = module.get<GridService>(GridService);
  });

  it("should generates the correct number of rows equal to the provided n", async () => {
    const n = 5;
    const gridConfigs = {
      rowSize: n,
      columnSize: n,
      initialChar: "#",
      altChar: " ",
    };
    const response = await service.generateGrid(gridConfigs);
    expect(response.trim().split("\n").length).toBe(n);
  });

  it("should generates a grid of the provided characters (*, -)", async () => {
    const n = 8;
    const gridConfigs = {
      rowSize: n,
      columnSize: n,
      initialChar: "*",
      altChar: "-",
    };
    const response = await service.generateGrid(gridConfigs);
    expect(response).toEqual(expect.stringMatching(/\*-/));
  });

  it("should generates a grid of the provided n size and with the provided characters (#, )", async () => {
    const n = 7;
    const gridConfigs = {
      rowSize: n,
      columnSize: n,
      initialChar: "#",
      altChar: " ",
    };
    const response = await service.generateGrid(gridConfigs);
    expect(response).toEqual(expect.stringMatching(/# /));
    expect(response.trim().split("\n").length).toBe(n);
  });

  it('should generate the default grid which consist of 8 column & 8 rows and uses the # & " " characters', async () => {
    const n = 8;
    const gridConfigs = {} as GenerateGridParams;
    const response = await service.generateGrid(gridConfigs);
    expect(response).toEqual(expect.stringMatching(/# /));
    expect(response.trim().split("\n").length).toBe(n);
  });
});
