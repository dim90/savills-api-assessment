import { Body, Controller, Logger, Post, Res } from "@nestjs/common";
import { Response } from "express";

import { GridService } from "./grid.service";
import { GridConfigs } from "./types";

@Controller("grid")
export class GridController {
  constructor(private gridService: GridService, private logger: Logger) {}

  response = "";

  @Post()
  generateGrid(@Body() gridConfigs: GridConfigs, @Res() res: Response) {
    try {
      this.logger.log("provided gridConfigs", JSON.stringify(gridConfigs));
      const generateGridParams = {
        rowSize: gridConfigs.n,
        columnSize: gridConfigs.n,
        initialChar: gridConfigs.x,
        altChar: gridConfigs.y,
      };

      const response = this.gridService.generateGrid(generateGridParams);
      res.set("Content-Type", "text/plain");
      res.status(200);
      res.send(response);
    } catch (error) {
      this.logger.error(error);
      res.status(500);
      res.send(error);
    }
  }
}
