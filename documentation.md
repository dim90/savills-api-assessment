# Documentation:

## Reasoning on the HTTP Verb:
- `POST` has been chosen as the HTTP verb and any character as `x`, `y` and `n` should be provided as part of the JSON body.
- `GET` params wasn't used because because I didn't want to limit the options of which characters could be used e.g `#` is consider a delimiter and represents the fragment section of a URI syntax (https://tools.ietf.org/html/rfc3986#section-3).
- `GET` query wasn't used either because in case special characters were used in the query string, encode would have been necessary and therefore a URL would end up looking like `localhost%3A3000%3Fx%3D%22%3A%22%26y%3D%22-%22` that is unreadable and difficult to amend.

## Request Params:
- They should be provided as key-value pairs in a JSON format in the body of the POST request  
`e.g {"n":8, "x":"#", "y":" "}`  
(Keys are optional, if any/or all of them are omitted they will default to the above values)

## How to query the endpoint:
- Make sure you have started the app - `npm run start`, you can either use:
     1. the provided postman collection (will need to import the 
     `savills-api-assessment.postman_collection.json` file to your local postman)
     2. The following curl command:  
      `curl -H "Content-Type: application/json" -X POST -d '{"n":8, "x":"#", "y":" "}' http://localhost:5000/grid`

## Testing & Code coverage
- `Unit tests` of the grid.service using Jest.
- `Integration tests` of grid.controller using Jest.
- `E2E tests` of the exposed endpoint `/grid` using Jest & Supertest.
- `Manual testing` of the exposed endpoint via Postman (savills-api-assessment.postman_collection.json included)
- `100% code coverage` of the 'grid' module