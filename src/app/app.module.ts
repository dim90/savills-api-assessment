import { Logger, Module } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";

import { GridModule } from "../grid/grid.module";

@Module({
  imports: [ConfigService, Logger, GridModule],
})
export class AppModule {}
