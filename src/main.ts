import { Logger } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { NestFactory } from "@nestjs/core";

import { AppModule } from "./app/app.module";

const main = async () => {
  const app = await NestFactory.create(AppModule);
  const config = app.get(ConfigService);
  const logger = app.get(Logger);

  const port = parseInt(config.get<string>("PORT") || "") || 5000;

  await app.listen(port);

  logger.log(`Listening on port: ${port}`);
};

main();
