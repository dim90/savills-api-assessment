import { Injectable } from "@nestjs/common";

import { GenerateGridParams } from "./types";

let grid = "";

@Injectable()
export class GridService {
  generateGrid({
    rowSize = 8,
    columnSize = 8,
    initialChar = "#",
    altChar = " ",
  }: GenerateGridParams): string {
    if (rowSize === columnSize) {
      grid = "";
    }
    let alterativeValue = rowSize % 2 === 0 ? initialChar : altChar;
    const swapValue = (value: string): string =>
      value === initialChar ? altChar : initialChar;

    if (rowSize === 0) {
      return grid;
    }

    let row = "";
    for (let i = 1; i <= columnSize; i++) {
      row += alterativeValue;
      alterativeValue = swapValue(alterativeValue);
    }

    grid += `${row}\n`;

    return this.generateGrid({
      rowSize: rowSize - 1,
      columnSize,
      initialChar,
      altChar,
    });
  }
}
