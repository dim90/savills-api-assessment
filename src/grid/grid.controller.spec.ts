import { Test, TestingModule } from "@nestjs/testing";
import { GridController } from "./grid.controller";
import { GridModule } from "./grid.module";
import { GridService } from "./grid.service";
import { Logger } from "@nestjs/common";
import { Response } from "express";
import { GridConfigs, GenerateGridParams } from "./types";

describe("Grid Controller", () => {
  let controller: GridController;
  let gridService: Partial<jest.Mocked<GridService>> = {};
  const logger: Partial<jest.Mocked<Logger>> = {
    log: jest.fn(),
    error: jest.fn(),
  };

  afterEach(() => {
    gridService = {};
    logger.log?.mockReset();
    logger.error?.mockReset();
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [GridModule],
    })
      .overrideProvider(GridService)
      .useValue(gridService)
      .overrideProvider(Logger)
      .useValue(logger)
      .compile();

    controller = module.get<GridController>(GridController);
  });

  it("should set the response Content-Type header to be text/plain, log the write message, call the gridService.generateGrid with the expected params, return a status 200, return the expected mock value", async () => {
    const { n, x, y }: GridConfigs = { n: 5, x: "#", y: " " };
    const res = ({
      set: jest.fn(),
      send: jest.fn(),
      status: jest.fn(),
    } as unknown) as Response;

    const mockedResponse: string = (x + y).repeat(n);
    const generateGridParams: GenerateGridParams = {
      altChar: y,
      columnSize: n,
      initialChar: x,
      rowSize: n,
    };
    gridService.generateGrid = jest.fn();
    gridService.generateGrid.mockImplementation(() => mockedResponse);

    await controller.generateGrid({ n, x, y }, res);
    expect(res.set).toHaveBeenCalledWith("Content-Type", "text/plain");
    expect(logger.log).toHaveBeenCalledWith(
      "provided gridConfigs",
      JSON.stringify({ n, x, y })
    );
    expect(gridService.generateGrid).toHaveBeenCalledWith(generateGridParams);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.send).toHaveBeenCalledWith(mockedResponse);
  });

  it("should return status 500 and the error when generateGrid service throws an error", async () => {
    const { n, x, y }: GridConfigs = { n: 5, x: "#", y: " " };
    const res = ({
      set: jest.fn(),
      send: jest.fn(),
      status: jest.fn(),
    } as unknown) as Response;

    gridService.generateGrid = jest.fn();
    const error = new Error("Testing Error");
    gridService.generateGrid.mockImplementation(() => {
      throw error;
    });

    await controller.generateGrid({ n, x, y }, res);
    expect(res.status).toHaveBeenCalledWith(500);
    expect(logger.error).toHaveBeenCalledWith(error);
    expect(res.send).toHaveBeenCalledWith(error);
  });
});
